from math import sqrt, log, cos, sin
import pylab
import numpy as np

def f(x, y, eps=0.1):
    return (1/eps)*(x - ((x**3)/3)) - (y/eps)

a_const = 1.1
a = a_const
def g(x, y, a=a_const):
    return x+a

x_rest = -a
y_rest = ((-3*a) + a**3)/3
INTENSE = 0.1
INTENSE1 = 0.04

print(a)
h = 0.001
k = 0.001
h_array = []

while k < 100.001:
     k+= 0.001
     h_array.append(k)

"""Начальные точки """
#node_1_2 = [[-1.17, -0.57], [-1.14, -0.575], [-1.180, -0.55], [-1.16, -0.59]]
focus_1_15 = [[-1.2160, -0.61], [-1.2165, -0.6145], [-1.2164, -0.6144], [-1.2163, -0.6141]] # узел
center_1a = [[-1, -0.66], [-0.68, -0.56], [-0.72, -0.53], [-0.74, -0.51]]
focus0_99b = [[-0.9, -0.65], [-0.62, -0.46], [-0.63, -0.43], [-0.64, -0.41]]
center_1 = [[-0.7, -0.52], [-0.68, -0.56], [-0.72, -0.53], [-0.74, -0.51]]
focus_1_2 = [[-1.2, -0.62], [-1.205, -0.621], [-1.202, -0.624], [-1.203, -0.623]]
node_1_8 = [[-2.9, 1.1], [-0.2, 1.1], [-0.82, 0.8], [-2.9, 0.8]]
node_3 = [[1.3, 0.7], [2, 0.7], [2, 0.3], [1.3, 0.3]]
afocus1_1 = [[-1.2, -0.8], [-1.2, -0.9], [-1.19, -0.75], [-1.2, -0.7]] #фигура 2, a
bfocus1_01 = [[-1, -0.78], [-1, -0.79], [-1, -0.75], [-1, -0.7]] #фигура 2, b
noise = [[x_rest, y_rest], [x_rest, y_rest], [x_rest, y_rest], [x_rest, y_rest]]

x_lists1 = []
y_lists1 = []
i_array = []

noise = [[x_rest, y_rest], [x_rest, y_rest], [x_rest, y_rest], [x_rest, y_rest]]
noise1 = [[x_rest, y_rest]]
array2 = noise1
for point_number in range(len(array2)):
    print(point_number)
    su = 0
    x_list1 = [array2[point_number][0]]
    y_list1 = [array2[point_number][1]]
    x_list2 = [array2[point_number][0]]
    y_list2 = [array2[point_number][1]]
    for i in range(1, 100000):
        pi = 3.14159
        rand_a = np.random.uniform() #случайные значения на отрезке [0, 1]
        rand_b = np.random.uniform()
        r1 = sqrt(-2 * log(rand_a)) * cos(2 * pi * rand_b) #чать случайной добавки к ym
        k1 = h * f(x_list1[i - 1], y_list1[i - 1])
        l1 = h * g(x_list1[i - 1], y_list1[i - 1])
        k2 = h * f(x_list1[i - 1] + (k1 / 2.0), y_list1[i - 1] + (l1 / 2.0))
        l2 = h * g(x_list1[i - 1] + (k1 / 2.0), y_list1[i - 1] + (l1 / 2.0))
        k3 = h * f(x_list1[i - 1] + (k2 / 2.0), y_list1[i - 1] + (l2 / 2.0))
        l3 = h * g(x_list1[i - 1] + (k2 / 2.0), y_list1[i - 1] + (l2 / 2.0))
        k4 = h * f(x_list1[i - 1] + k3, y_list1[i - 1] + l3)
        l4 = h * g(x_list1[i - 1] + k3, y_list1[i - 1] + l3)

        k1t = h * f(x_list2[i - 1], y_list2[i - 1])
        l1t = h * g(x_list2[i - 1], y_list2[i - 1])
        k2t = h * f(x_list2[i - 1] + (k1t / 2.0), y_list2[i - 1] + (l1t / 2.0))
        l2t = h * g(x_list2[i - 1] + (k1t / 2.0), y_list2[i - 1] + (l1t / 2.0))
        k3t = h * f(x_list2[i - 1] + (k2t / 2.0), y_list2[i - 1] + (l2t / 2.0))
        l3t = h * g(x_list2[i - 1] + (k2t / 2.0), y_list2[i - 1] + (l2t / 2.0))
        k4t = h * f(x_list2[i - 1] + k3t, y_list2[i - 1] + l3t)
        l4t = h * g(x_list2[i - 1] + k3t, y_list2[i - 1] + l3t)

        xt = x_list2[i - 1] + ((1.0 / 6) * (k1t + 2.0 * k2t + 2.0 * k3t + k4t))
        yt = y_list2[i - 1] + ((1.0 / 6) * (l1t + 2.0 * l2t + 2.0 * l3t + l4t)) + INTENSE1 * (
                    sqrt(h) * r1)  # intense = интенс шума

        xm = x_list1[i - 1] + ((1.0 / 6) * (k1 + 2.0 * k2 + 2.0 * k3 + k4))
        ym = y_list1[i - 1] + ((1.0 / 6) * (l1 + 2.0 * l2 + 2.0 * l3 + l4)) + INTENSE*(sqrt(h) * r1) #intense = интенс шума

        x_list1.append(xm)
        y_list1.append(ym)

        x_list2.append(xt)
        y_list2.append(yt)




pylab.plot(h_array, x_list1)
pylab.xlabel('t')
pylab.ylabel('Значение Х')
pylab.plot(h_array, x_list2, color='red') #маленькая интенсивность INTENSE1

print('ПЕРВЫЙ', x_list1)
print('ВТОРОЙ', x_list2)

# Покажем окно с нарисованным графиком
pylab.show()